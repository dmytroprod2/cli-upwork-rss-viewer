const path = require("path");
const readline = require('readline');

let Parser = require('rss-parser');
let parser = new Parser();

const listApp = require(path.join(__dirname, "listModel"));


const getFeed = async (url) => {
    let feed = await parser.parseURL(url);
    return feed
};


const questionApp = (rl, text) => {
    return new Promise((resolve) => {
        rl.question(text, (answer) => {
            resolve(answer)
        })
    })
};


const main = async () => {
    //{vendorId,productCode}
    let workId = process.argv[2]

    if (workId === undefined) {
        console.info("if you know rss id you can pass it as first parameter e.g.'node cli_sortRss.js 0'")
        console.info("available RSS names")
        console.info("id / name")
        listApp.showRssList()
        const rl = readline.createInterface({input: process.stdin, output: process.stdout});
        workId = await questionApp(rl, "please provide id of the rss: ");

        rl.close()
    }

    const listResp = listApp.getRssById(workId)
    if(!listResp.found){
        console.info("no RSS was found for id " + workId)
        return
    }

    const {name,url} = listResp;
    console.info("RSS name applied ", name);
    console.info("RSS url ", url);

    const feed = await getFeed(url)

    const r2 = readline.createInterface({input: process.stdin, output: process.stdout});
    let action, priority;

    for (let item of feed.items) {
        let link = item.link
        if(listApp.isInProcessed(link)){
            console.info("### ============= processed " + item.title);
        }else{
            console.info("------------------");
            console.info(item.title);
            console.info("------------------");
            console.info(link);
            console.info("------------------");
            console.info(item["content:encodedSnippet"]);
            console.info("------------------");
            console.info(item.content);
            console.info("------------------");
            action = await questionApp(r2, "\n decide what to do with this job. your decision will be saved and applied for this job next time \n x - skip \t y - apply \t q - exit now: ");
            console.info("action ", action);
            console.info("\n\n\n\n");
            if (action === "q") {
                break
            }
            if(action==="y"){
                priority = await questionApp(r2, "set apply priority 1-3:  ");
                listApp.appendApply(name, priority,item.title, link)
            }
            listApp.appendProcessed(name,action,item.title,link)
        }
    }
    r2.close()
    console.info("listOfApply.txt and listOfProcessed.txt appended according to your decisions")
}

main()
