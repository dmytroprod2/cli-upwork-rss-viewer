'use strict';
const { exec } = require( 'child_process' );

const path = require("path");
const readline = require('readline');
const listApp = require(path.join(__dirname, "listModel"));

const questionApp = (rl, text) => {
    return new Promise((resolve) => {
        rl.question(text, (answer) => {
            resolve(answer)
        })
    })
};


const showSummary = (urlsResp)=>{
    console.info(`priority-jobs   `)
    for(let name in urlsResp) {
        let sum = 0
        console.info(name)

        for (let priority in urlsResp[name]) {
            let len = urlsResp[name][priority].length
            sum = sum + len;
            if (len > 0) {
                console.info(`${priority}-${len}`)
            }
        }
        console.info(name + "  total: " + sum)
        console.info("---------")
    }
}

const getUrlsBuyNameAndPriorities = (urlResp, name, priority)=>{
    //console.info(JSON.stringify(urlResp),null,3)
    let urls = []

    const toShowByName = (name,elName)=>{
        return (name==="-a")?true:(name===elName)
    }

    const toShowByPriority = (priority,elpriority)=>{
        return (priority==="-a")?true:(String(priority)===String(elpriority))
    }

    for (let elName in urlResp){
        if(!toShowByName(name,elName)){
            continue
        }else{
            for(let elPriority in urlResp[elName]){
                //console.info(elName)
                //console.info(elPriority)
                if(toShowByPriority(priority,elPriority)){
                    let urlsArr = urlResp[elName][String(elPriority)]
                    if(urlsArr.length!==0){
                        //console.info(urlsArr)
                        let urlsInArr = urlsArr.map(el=>{return el[1]})
                        //console.info(urlsInArr)
                        urls = urls.concat(urls,urlsInArr)
                        urls = urls.filter((url,id,urls)=>{return id===urls.indexOf(url)});
                    }
                }
            }
        }
    }
    return urls
}

const main = async () => {
    //{vendorId,productCode}
    let name = process.argv[2]
    let priority = process.argv[3]

    const urlsResp = listApp.getUrlsInApply()
    console.info("\nSummary of jobs by name and priority\n")
    showSummary(urlsResp);

    if(name==="-a"){
        priority = "-a"
    }else if (name === undefined || priority == undefined) {
        console.info("you can pass name and priority as parameter e.g.'node cli_openInChrome.js nodeJs -a'")
        const rl = readline.createInterface({input: process.stdin, output: process.stdout});
        name = await questionApp(rl, "please provide name (-a for all): ");
        if(urlsResp[name]===undefined){
            console.info("no jobs with name " + name)
            rl.close()
            return
        }else{
            priority = await questionApp(rl, "please provide priority (-a for all): ");
            rl.close()
            if(priority === "-a"){
                //ok
            }else if(urlsResp[name][priority]===undefined){
                console.info("you provided : " + priority + " Only 1-3 are allowed")
                return
            }

        }
    }

    const urls = getUrlsBuyNameAndPriorities(urlsResp,name,priority)
    console.info("urls found : ", urls.length)
    console.info("urls", urls)

    urls.forEach((url=>{
        exec('start chrome '+url)
    }))

}

main()

//console.log(getUrlsBuyNameAndPriorities(listApp.getUrlsInApply(),"zapier","-a"))

