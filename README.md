#### This CLI was developed to make jobs sorting and applying flow in Upwork comfortable. You can see jobs in CLI and sort then. The tool remember yout choice and you'll pay attention to new jobs only.  

node and npm shall be installed. Chrom browser shall be installed.

## Do it once.
- clone project.
- run `npm install` to install dependencies.
- run `node cli_init` to create folder and files.  
the folder `cli-upwork-rss-viewer-list-storage` will be created next to this project folder.

## Adding skills  to RSS List 
In `cli-upwork-rss-viewer-list-storage\rssList.txt` add rss URLs and it's custom space-less short name separated with one spaces.
here is a sample of the row for the file.   
`nodejs https://www.upwork.com/ab/feed/jobs/rss?q=node+js&sort=recency&paging=0%3B50`

## Select jobs to apply reading descriptions in CLI
run `node cli_sortRss` and follow the instructions to sort jobs by skills and priorities.

## Open jobs' pages in Chrome all or filtered at once from CLI. 
run `node cli_openInChrome` to open filtered by skill and priority jobs in Chrome.  



