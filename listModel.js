const fs = require('fs');
const path = require('path');

const storagePath = path.join(__dirname,"..","cli-upwork-rss-viewer-list-storage")

const processedListPath = path.join(storagePath,"listOfProcessed.txt")
const applyListPath = path.join(storagePath,"listOfApply.txt")
const rssListPath = path.join(storagePath,"rssList.txt")

const initFolderAndFiles = ()=>{
    const folderHandler = (name, path) => {
        const checker = fs.existsSync(path)
      //  console.log(checker)
        if (checker) {
            console.info(`exists \t${name} `)
        } else {
            fs.mkdirSync(path)
            console.info(`created \t${name} `)
        }
    };

    const filesHandler = (fileName)=>{
        const filePath = path.join(storagePath,fileName)
        if(fs.existsSync(filePath)){
            console.info(`file exists ${fileName} `)
        }else {
            fs.writeFileSync(filePath,``)
            console.info(`file created ${fileName}`)
        }
    };

    folderHandler("storage folder",storagePath);

    filesHandler("listOfProcessed.txt")
    filesHandler("listOfApply.txt")
    filesHandler("rssList.txt")

    console.info("\nall folders and files are ready now");

}

const appendProcessed =  (name, action, title, url)=>{
    fs.appendFileSync(processedListPath, `${(new Date()).toISOString()}\t${name}\t${action}\t${title}\t${url}\n`, 'utf8');
}

const appendApply = (name,priority, title,url)=>{
    fs.appendFileSync(applyListPath, `${(new Date()).toISOString()}\t${name}\t${priority}\t${title}\t${url}\n`, 'utf8');
}

const isInProcessed = (url)=>{
    let txt = fs.readFileSync(processedListPath, 'utf8');
    return txt.includes(url)
}

const isInApply = (url)=>{
    let txt = fs.readFileSync(applyListPath, 'utf8');
    return txt.includes(url)
}

const showRssList = ()=>{
    let txt = fs.readFileSync(rssListPath, 'utf8');
    let strs = txt.split("\n");
    strs.forEach((str,id)=>{console.info(id,str.split(" ")[0])})
}

const getRssById = (id)=>{
    let txt = fs.readFileSync(rssListPath, 'utf8');
    let strs = txt.split("\n");
    let row = strs.filter((el,elId)=>{return String(id)===String(elId)})
    if(row.length===0){
        return {
            found:false
        }
    }
    const [name, url] = row[0].split(" ").map(el=>{return el.trim()})
    return {
        found: true,
        name,
        url
    }
}

//showRssList()
// console.log(getRssById(0))
//console.log(getRssById(8))

const getUrlsInApply = ()=>{
    let txt = fs.readFileSync(applyListPath, 'utf8');
    let strs = txt.split("\n");
    let urls = {}
    for(let str of strs){
        let [date,name,priority, title,url] = str.split("\t")
        if(date===""){continue}
        console.log({date,name,priority, title,url})
        if(urls[name]===undefined){
            urls[name]={
                "1":[],
                "2":[],
                "3":[]
            }
        }
        urls[name][String(priority)].push([
            title,
            url
        ])
    }
    return urls
    //{"nodeJs":{"1":[["Microsoft Teams application developer - Upwork","https://www.upwork.com/jobs/Microsoft-Teams-application-developer_%7E01a696140d43a6586a?source=rss"]],"2":[],"3":[]}}
}

module.exports ={
    appendProcessed,
    appendApply,
    isInProcessed,
    isInApply,
    showRssList,
    getRssById,
    getUrlsInApply,
    initFolderAndFiles
}

